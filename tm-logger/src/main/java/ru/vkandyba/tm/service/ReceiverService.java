package ru.vkandyba.tm.service;

import lombok.SneakyThrows;

import javax.jms.*;

public class ReceiverService {

    private final ConnectionFactory connectionFactory;

    public ReceiverService(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @SneakyThrows
    public void receive(final MessageListener listener){
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createTopic("TM");
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(listener);

    }
}
