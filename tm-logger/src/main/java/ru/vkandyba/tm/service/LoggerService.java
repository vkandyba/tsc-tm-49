package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import ru.vkandyba.tm.dto.EntityLogDTO;

import java.io.File;
import java.io.FileOutputStream;

public class LoggerService {

    @SneakyThrows
    public void writeLog(final EntityLogDTO message){
        final String className = message.getClassName();
        final String fileName = getFileName(className);
        if(fileName == null) return;
        final File file = new File(fileName);
        file.getParentFile().mkdirs();
        final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        final String header = "Id: " + message.getId() + "; Type: " + message.getType() + "; Date: " + message.getDate() + "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(final String className){
        switch (className){
            case "ProjectDTO":
            case "Project":
                return "PROJECT_LOG";
            case "TaskDTO":
            case "Task":
                return "TASK_LOG";
            case "UserDTO":
            case "User":
                return "USER_LOG";
            case "SessionDTO":
            case "Session":
                return "SESSION_LOG";
            default:
                return null;
        }

    }


}
