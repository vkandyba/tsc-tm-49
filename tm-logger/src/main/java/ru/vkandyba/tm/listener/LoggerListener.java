package ru.vkandyba.tm.listener;

import lombok.SneakyThrows;
import ru.vkandyba.tm.dto.EntityLogDTO;
import ru.vkandyba.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class LoggerListener implements MessageListener {

    final LoggerService loggerService = new LoggerService();

    @SneakyThrows
    @Override
    public void onMessage(Message message) {;
        if(!(message instanceof ObjectMessage)) return;
        final Serializable entity = ((ObjectMessage) message).getObject();
        if(entity instanceof EntityLogDTO) loggerService.writeLog((EntityLogDTO) entity);
    }
}
