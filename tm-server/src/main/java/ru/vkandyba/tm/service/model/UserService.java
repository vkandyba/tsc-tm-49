package ru.vkandyba.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyLoginException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.exception.empty.EmptyPasswordException;
import ru.vkandyba.tm.model.User;
import ru.vkandyba.tm.repository.model.UserRepository;
import ru.vkandyba.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class UserService extends AbstractService {
    
    public UserService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    public User findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            final UserRepository repository = new UserRepository(entityManager);
            return repository.findById(id);
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            final UserRepository repository = new UserRepository(entityManager);
            return repository.findByLogin(login);
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void updateUser(@Nullable String userId, @Nullable String firstName,
                           @Nullable String lastName, @Nullable String middleName)
    {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyIdException();
        if(lastName == null || lastName.isEmpty()) throw new EmptyNameException();
        if(middleName == null || middleName.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final UserRepository repository = new UserRepository(entityManager);
            repository.updateUser(userId, firstName, lastName, middleName);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if(password == null || password.isEmpty()) throw new EmptyPasswordException();
        if(email == null || email.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final UserRepository repository = new UserRepository(entityManager);
            final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(connectionService.getPropertyService(), password));
            user.setEmail(email);
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void removeByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final UserRepository repository = new UserRepository(entityManager);
            repository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final UserRepository repository = new UserRepository(entityManager);
            repository.lockUserByLogin(login);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final UserRepository repository = new UserRepository(entityManager);
            repository.unlockUserByLogin(login);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
    
}
