package ru.vkandyba.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectService extends AbstractService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    public List<Project> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.findAll(userId);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    @Nullable
    public Project findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public Project findById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.findByName(userId, id);
        } catch (Exception e){
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void add(String userId, Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.removeByName(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.updateById(userId, id, name, description);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.startById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.startByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.finishById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    
    public void finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.finishByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
