package ru.vkandyba.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.component.Bootstrap;
import ru.vkandyba.tm.enumerated.EntityOperationType;

import javax.persistence.*;

@NoArgsConstructor
public class EntityListener {

    @PostLoad
    public void postLoad(@NotNull final Object entity){
        sendMessage(entity, EntityOperationType.POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity){
        sendMessage(entity, EntityOperationType.PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity){
        sendMessage(entity, EntityOperationType.POST_PERSIST);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity){
        sendMessage(entity, EntityOperationType.PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity){
        sendMessage(entity, EntityOperationType.POST_UPDATE);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity){
        sendMessage(entity, EntityOperationType.PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity){
        sendMessage(entity, EntityOperationType.POST_REMOVE);
    }

    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType){
        Bootstrap.sendMessage(entity, operationType.toString());
    }
}
