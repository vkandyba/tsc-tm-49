package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    SessionDTO openSession(
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    );

}