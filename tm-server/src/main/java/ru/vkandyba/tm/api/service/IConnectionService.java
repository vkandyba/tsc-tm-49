package ru.vkandyba.tm.api.service;

import org.apache.ibatis.session.SqlSession;

import javax.persistence.EntityManager;
import java.sql.Connection;

public interface IConnectionService {

    IPropertyService getPropertyService();

    SqlSession getSqlSession();

    EntityManager getEntityManager();

}
