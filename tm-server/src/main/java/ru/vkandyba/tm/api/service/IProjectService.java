package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectService{

    List<ProjectDTO> findAll(String userId);

    ProjectDTO findByName(String userId, String name);

    ProjectDTO findById(String userId, String name);

    void add(String userId, ProjectDTO project);

    void removeByName(String userId, String name);

    void removeById(String userId, String id);

    void updateById(String userId, String id, String name, String description);

    void startById(String userId, String id);

    void startByName(String userId, String name);

    void finishById(String userId, String id);

    void finishByName(String userId, String name);

    void clear(String userId);

}
