package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void createUser(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    void removeUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void lockUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void unLockUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );
}
